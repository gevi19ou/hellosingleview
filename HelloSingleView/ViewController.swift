//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Vivien Geschwind on 23.10.19.
//  Copyright © 2019 Vivien Geschwind. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func clickButton(_ sender: Any) {
        print("Hello button")
    }
    
}

